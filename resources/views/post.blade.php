@extends('layouts.app')
@section('content')
    <div class="container-fluid app-body post-page">
			<div class="row">
				<div class="col-md-12">
					<div class="panel panel-default">
						 <div class="panel-heading">Recent Posts</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-md-3">
									<input type="text" class="form-control" id="search">
								</div>
								<div class="col-md-3"><input type="text" class="form-control" id="date"></div>
								<div class="col-md-3">
									  <select class="form-control" id="group">
								        <option>All Group</option>
								        <option value="upload">Content Upload</option>
								        <option value="curation">Content Curation</option>
								        <option value="rss-automation">RSS Automation</option>
								      </select>
								</div>
								<div class="col-md-3"></div>
								
							</div>
							<div class="row" style="margin-top:20px ">
									<div class="col-md-12">
										  <table class="table table-condensed" id="post-table">
										    <thead>
										      <tr>
										        <th>Group Name</th>
										        <th>Group Type</th>
										        <th>Account Name</th>
										        <th>Post Text</th>
										        <th>Time</th>
										      </tr>
										    </thead>
										    <tbody>
										    	@foreach($posts as $post)
										      <tr>
										      	 <td>{{$post->groupInfo->name}}</td>
										      	 <td>{{$post->groupInfo->type}}</td>
										      	 <td><img src="{{$post->accountInfo->avatar}}" /> </td>
									        	<td>{{$post->post_text}}</td>
									        	<td>{{$post->sent_at}}</td>
										        </tr>
										   
										      @endforeach
										  </tbody>
										</table>
										{{ $posts->links() }}
														    
									</div>
							</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
@endsection