<?php

namespace Bulkly\Http\Controllers;

use Illuminate\Http\Request;

use Bulkly\BufferPosting;
use Bulkly\SocialPostGroups;

class PostController extends Controller
{
    public function index()
    {
    	$posts=BufferPosting::paginate(30);
    	return view('post', compact('posts'));
    }
    public function groupsearch( Request $request)
    {
    	$groups=SocialPostGroups::where('type','Like', $request->text)->pluck('id');
    	$posts=BufferPosting::whereIn('group_id', $groups)->paginate(10); 
    	if($posts){
     		$output="";
           foreach ($posts as  $post) {
           
            $output.='<tr>'.
            
            '<td>'.$post->groupInfo->name.'</td>'.
            
            '<td>'.$post->groupInfo->type.'</td>'.
            '<td>'.$post->accountInfo->avatar.'</td>'.
            
            //'<td> <img src='.$post->accountInfo->avatar.'></td>'.
            
            '<td>'.$post->post_text.'</td>'.
            '<td>'.$post->sent_at.'</td>'.
            '</tr>';
           }
           return $output;  
    	
    	
    }
}
}
